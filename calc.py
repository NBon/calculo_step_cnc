# Calculo para determinar passos reis entre mootor e cremalheira para CNC

d = float(input('Digite a medida optida: '))
e = float(input('Digite a medida de apoio: '))
b = float(input('Digite a resolução de passos no programa: '))
c = float(input('Digite a medida pretendida dos passos: '))

# calculo das medidas reais
a = d - e

# calculo para obter a resolução necessaria
x = a * b / c

print(f'Alterar resolução de passos {b} por: {x:.6f}\n')